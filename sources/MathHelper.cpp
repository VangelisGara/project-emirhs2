#include <string>
#include <chrono>
#include <random>
#include <iostream>
#include <math.h>
#include "../headers/MathHelper.h"

using namespace std;

// return a random number based on uniform distribution
double uniformDistribution(double a,double b){ // [a,b)
  random_device rd;  // get seed
  mt19937 gen(rd()); // standard mersenne_twister_engine seeded with rd()
  uniform_real_distribution<double> t_prod(a, b);
  return t_prod(gen);
}

// return a v point with dim dimension , randomly filled based on normal distribution
float normalDistribution(){
  unsigned seed = chrono::system_clock::now().time_since_epoch().count(); // get seed
  default_random_engine generator(seed);
  normal_distribution<float> distribution (0.0,1.0);
  return distribution(generator);
}

// the dot product of two points
double dotProduct(vector<double>* a, vector<double>* b){
  int dimension = (*a).size();
  double sum=0;
  for(int i=0; i<dimension; i++){
    sum += (*a)[i] * (*b)[i];
  }
  return sum;
}

// euclidean distance between two points
double euclidean_distance(vector <double>* a,vector <double>* b){
  int dimensions = (*a).size();
  double sum = 0;
  for(int i=0 ; i<dimensions ; i++){
    double sum0 = (*a)[i] - (*b)[i];
    sum0 = sum0*sum0;
    sum += sum0;
  }
  double distance = sqrt(sum);
  return distance;
}

// cosine distance betweem two points
double cosine_distance(vector <double> *a,vector <double> *b){
    double dot = 0.0, denom_a = 0.0, denom_b = 0.0 ;
     for(unsigned int i = 0u; i < (*a).size(); ++i) {
        dot += (*a)[i] * (*b)[i] ;
        denom_a += (*a)[i] * (*a)[i] ;
        denom_b += (*b)[i] * (*b)[i] ;
    }
    double cosine_similarity = dot / (sqrt(denom_a) * sqrt(denom_b)) ;
    double distance = 1 - cosine_similarity;
}

// haming distance between two integers
int hamming_distance(int x, int y){
  return __builtin_popcount(x^y);
}

// convert binary to integer , where binary is stored into a vector
int binary_vectorToDecimal(vector<int>& binary){
    int decimal = 0 , base = 1;
    for(int i=binary.size()-1; i>=0; i--){
        decimal += binary[i]*base;
        base *= 2;
    }
    return decimal;
};
