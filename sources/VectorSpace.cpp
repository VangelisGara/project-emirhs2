#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include "../headers/VectorSpace.h"
#include "../headers/MathHelper.h"
#include "../headers/Insights.h"

using namespace std;

// vector space constructor
VectorSpace::VectorSpace(string input_file){
  ifstream in_file;
  string point,coordinate;
  int i = 0, num_points = 0 ,dimen = 0;
  double coord;

  // open file
  in_file.open(input_file);
  if(in_file.fail()){
    cout << "Error opening file: " << input_file << '\n';
    exit(-1);
  }

  // for every line
  while(! in_file.eof()){
    getline(in_file,point);
    if(point.length() == 0) // ignore empty lines
       continue;
    Dspace.push_back(vector <double>()); // push back an empty vector
    istringstream iss(point); // make line a string stream
    i = 0;
    // tokenize line to get each coordinate
    while(getline(iss,coordinate,',')){
      if( i == 0)
        itemIds.push_back(stoi(coordinate));
      else{
        Dspace[num_points].push_back(stod(coordinate));
      }
      i++;
    };
    if(i > 1) // number of coordinates in line
      dimen = i-1;
    num_points++; // next line now , so new point
    // calculate memory allocated for the storage of one point
    memory_usage += sizeof(vector<double>) + (sizeof(double) * Dspace[num_points-1].size());
  }

  // set informatiom about vector space
  number_of_points = num_points;
  dimension = dimen;

  // finalize
  in_file.close();

  // Calculate memory use for the whole vector space
  memory_usage += sizeof(vector<int>) + (sizeof(int)* itemIds.size()); // memory usage to store the id of each point
  memory_usage += sizeof(vector<vector <double>>); // add the size of vector that contains all those smaller vectors
}

// vector space destructor
VectorSpace::~VectorSpace()
{
    //cout << "Vector space will be destroyed soon" << '\n';
}

// Getters
int VectorSpace::getDimension(){
  return dimension;
}

int VectorSpace::getNumberOfPoints(){
  return number_of_points;
}

vector<int>* VectorSpace::getIds(){
  return &itemIds;
}

// returns the id of the nearest neighbor
int VectorSpace::NearestNeighbor(vector <double>* q,string metric){
  double temp , min = 1000000.0;
  int nn, nup = number_of_points, dim = dimension;
  if(metric == "euclidean"){
    for(int i=0; i< nup ; i++){
      temp = euclidean_distance(&(Dspace[i]),q);
      if(temp < min){
        min = temp;
        nn = itemIds[i];
      }
    }
  }
  else if(metric == "cosine"){
    for(int i=0; i< nup ; i++){
      temp = cosine_distance(&(Dspace[i]),q);
      if(temp < min){
        min = temp;
        nn = itemIds[i];
      }
    }
  }
  //cout << "distanceTrue: " << min << endl;
  return nn;
}

// Print a point
void VectorSpace::printPoint(int i){
  cout << itemIds[i] << endl;
  for(int j=0; j<dimension ; j++)
    cout << Dspace[i][j] << " ";
  cout << endl;
}

// Print vector space elements
void VectorSpace::printVectorSpace(){
  int nup = number_of_points;
  int dim = dimension;
  cout << "Vector space with " << nup << " , "<< dim << " dimensions" << endl;
  for(int i=0; i< nup ; i++){
    cout << "--------------------------" << endl;
    cout << itemIds[i] << endl;
    for(int j=0; j<dimension ; j++)
      cout << Dspace[i][j] << " ";
    cout << endl;
  }

  cout << " Vector space has " << number_of_points << " points and it has " << dimension << " dimensions" << endl;
}
