#include <vector>
#include <string>
#include <random>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include "../headers/VectorSpace.h"
#include "../headers/HashTable.h"
#include "../headers/MathHelper.h"
#include "../headers/Insights.h"

using namespace std;

// Hashed Point contructor
HashedPoint::HashedPoint(int indx){
  index = indx; // the point's index in vector space ( the p point )

  memory_usage = sizeof(int) + sizeof(vector<int>); // memory usage for hashed point
}

// Hashed Point constructor , for hash tables that should store the g function
HashedPoint::HashedPoint(int indx , vector<int>* g_func , int k){
  index = indx; // the point's index in vector space ( the p point )
  for(int i=0; i<k ; i++)
    g.push_back((*g_func)[i]); // the g function that the point was hashed

  memory_usage += sizeof(int) + sizeof(vector<int>) + (sizeof(int)*g.size()); // memory usage for hashed point
}

// Hashed Point destructor
HashedPoint::~HashedPoint(){

}

// Getters
int HashedPoint::getIndex(){
  return index;
}

vector<int> HashedPoint::getG(){
  return g;
}

// Print contents of hash point
void HashedPoint::printHashedPoint(){
  cout << "Index: " << index << " "; // points index in vector space
  cout << "g:" ; // the g function that hashed from
  for(int i=0; i< g.size(); i++)
    cout << g[i] << " ";
  cout << endl;
}

// Hash Table constructor
HashTable::HashTable(int k,vector<int> initial_r,int dim){
  srand(time(NULL));
  r = initial_r; // initialize r family of this hash table
  for(int i=0; i<k ; i++){
    //pick single-precision real t uniformly ∈R [0, w )
    t.push_back( uniformDistribution(0.0,w*1.0) );

    // Let d-vector have coordinates identically independently distributed (i.i.d.) by the standard normal
    v.push_back(vector <double>()); // push back an empty vector
    for(int j=0; j<dim; j++){
      v[i].push_back( normalDistribution() );
      memory_usage += sizeof(vector<double>) + (sizeof(double)*v.size()); // memory usage for stl vector that stores v vector
    }
  }

  memory_usage += sizeof(double);
  memory_usage += sizeof(vector<float>) + (sizeof(float)*t.size()); // memory usage for stl vector that stores r family
  memory_usage += sizeof(vector<int>) + (sizeof(int)*r.size()); // memory usage for stl vector that stores r family
}

// Hash Table destructor
HashTable::~HashTable(){
}

// The LSH family : euclidean
int HashTable::euclidean(vector <double>* p, int i){
  // dot product
  double pv = dotProduct(p,&(v[i]));
  // Essentially project p on the line of v, shift by t, partition into cells of length w.
  int hi = floor((pv+t[i])/w);

  return hi;
}

// The LSH family : cosine
int HashTable::cosine(vector <double>* p,int i){
  // dot product
  double rx = dotProduct(p,&(v[i])); // r double vector

  if(rx >= 0)
    return 1;
  else
    return 0;
}

// Hash a point and store it into the hash table
void HashTable::HashPoint(int k ,int indx , vector <double>* p,int n,string metric,string alg){

  if(alg == "lsh"){
    if(metric == "euclidean"){
      // create g[h1,h2,...,hk] hash function
      vector<int> g;
      for(int i=0; i<k ; i++){
        int hi = euclidean(p,i);
        g.push_back(hi);
      }

      memory_usage += sizeof(vector<int>) + (sizeof(int)*g.size()); // memory usage for stl vector that stores g functions

      // convert k-dimensional hashtable yielded to an integer index
      unsigned int M = pow(2,32) - 5;
      int TableSize = n/4;
      long long int Sum_riki=0;
      unsigned int Fi=0;
      for(int i=0; i<k ; i++){
        Sum_riki = r[i]*g[i];
        Fi += ((Sum_riki % M) + M)%M;
      }
      Fi = ((Fi % TableSize) + TableSize)%TableSize;

      // insert to hash table
      HashedPoint hp(indx,&g,k); // create a hashed point
      this->umap.insert(make_pair(Fi,hp)); // insert it to hash table

      memory_usage += sizeof(int); // memory usage for key
    }

    else if(metric == "cosine"){
      // create g[h1,h2,...,hk] hash function
      vector<int> g;
      for(int i=0; i<k ; i++){
        int hi = cosine(p,i);
        g.push_back(hi);
      }

      memory_usage += sizeof(vector<int>) + (sizeof(int)*g.size()); // memory usage for stl vector that stores g functions

      //convert binary to key
      int key = binary_vectorToDecimal(g);
      //cout << key << endl;

      // insert hash point
      HashedPoint hp(indx,&g,k); // create a hashed point
      this->umap.insert(make_pair(key,hp)); // insert it to hash table

      memory_usage += sizeof(int); // memory usage for key
    }
  }

  else if(alg == "cube"){
    //cout << "I will hash point with " << alg << " and metric " << metric << endl;
    vector<int> f;
    int d2 = floor(log2(k)); // k will be n for hybercube , in order to compute d'
    //cout << d2 << endl;
    for(int i=0 ; i<d2; i++){
      if(metric=="euclidean"){
        int fi, hi;
        hi = euclidean(p,i);
        fi = ((hi % 2) + 2)%2;
        f.push_back(fi);
      }
      else if(metric=="cosine"){
        int fi, hi;
        hi = cosine(p,i);
        fi = hi;
        f.push_back(fi);
      }
    }

    memory_usage += sizeof(vector<int>) + (sizeof(int)*f.size()); // memory usage for stl vector that stores f functions

    //convert binary to key
    int key = binary_vectorToDecimal(f);
    //cout << key << endl;

    // insert to hash table
    HashedPoint hp(indx); // create a hashed point
    this->umap.insert(make_pair(key,hp)); // insert it to hash table

    memory_usage += sizeof(int); // memory usage for key
  }
}

// Hash all points in a vector space
void HashTable::HashVectorSpace(VectorSpace* D,int k,string metric,string alg){
  int nup = D->getNumberOfPoints();
  int dim = D->getDimension();
  vector <int>* ids = D->getIds();
  //cout << "Hash vector space with " << nup << " , " << dim << " dimensions" << endl;
  for(int i=0; i< nup ; i++)
    HashPoint(k,i,&(D->Dspace[i]),nup,metric,alg);

  memory_usage += sizeof(umap); // memory usage for the undorder multimap
}

// Return all point within radius R from point q
vector<int> HashTable::RangeSearch(double R,VectorSpace* D,vector <double>* q,int k,int n,string metric,int c){
  vector <int> NearestNeighbors;
  if(metric == "euclidean"){
    // hash the q point in order to have access to bucket
    vector<int> g;
    for(int i=0; i<k ; i++){
      int hi = euclidean(q,i);
      g.push_back(hi);
    }
    memory_usage += sizeof(vector<int>) + (sizeof(int)*g.size()); // memory usage for stl vector that stores g functions

    // convert k-dimensional hashtable , yielded , to an integer index
    unsigned int M = pow(2,32) - 5;
    int TableSize = n/4;
    long long int Sum_riki=0;
    unsigned int Fi=0;
    for(int i=0; i<k ; i++){
      Sum_riki = r[i]*g[i];
      Fi += ((Sum_riki % M) + M)%M;
    }
    Fi = ((Fi % TableSize) + TableSize)%TableSize;

    // for each item in bucket
    auto range = umap.equal_range(Fi);
    for (unordered_multimap <int,HashedPoint>::iterator it=range.first; it!=range.second; ++it){
      if( g == it->second.getG()){ // g(q) == g(p)
        int point = it->second.getIndex(); // get the point in bucket
        double dist = euclidean_distance(&((*D).Dspace[point]),q); // the euclidean distance between p and q
        //cout << "Point : " << point << " , Euclidean distance : " << dist << endl;

        if( R == 0)
          NearestNeighbors.push_back(point);
        if(dist < R)
          NearestNeighbors.push_back(point);
      }
    }

    memory_usage += sizeof(vector<int>) + (sizeof(int)*NearestNeighbors.size()); // memory usage for stl vector that stores nns

    return NearestNeighbors;
  }

  else if(metric == "cosine"){
    // hash the q point in order to have access to bucket
    vector<int> g;
    for(int i=0; i<k ; i++){
      int hi = cosine(q,i);
      g.push_back(hi);
    }

    memory_usage += sizeof(vector<int>) + (sizeof(int)*g.size()); // memory usage for stl vector that stores g functions

    //convert binary to key
    int key = binary_vectorToDecimal(g);

    // for each item in bucket
    auto range = umap.equal_range(key);
    for(unordered_multimap <int,HashedPoint>::iterator it=range.first; it!=range.second; ++it){
      int point = it->second.getIndex(); // get the point in bucket
      double dist = cosine_distance(&((*D).Dspace[point]),q); // the euclidean distance between p and q
      //cout << "Point : " << point << " , Cosine distance : " << dist << endl;
      if( R == 0)
        NearestNeighbors.push_back(point);
      if(dist < R)
        NearestNeighbors.push_back(point);
    }

    memory_usage += sizeof(vector<int>) + (sizeof(int)*NearestNeighbors.size()); // memory usage for stl vector that stores nns

    return NearestNeighbors;
  }
}

// Return all point within radius R from point q
vector<int> HashTable::RangeSearchCube(double R,VectorSpace* D,vector <double>* q,int M,int probes,int k,string metric){
  vector <int> NearestNeighbors;
  vector<int> f;
  vector<int> VerticesChecked;
  int current_M=M, current_probes=probes;
  int d2 = floor(log2(k)); // k will be n for hybercube , in order to compute d'
  //cout << d2 << endl;

  // create f function
  for(int i=0 ; i<d2; i++){
    if(metric=="euclidean"){
      int fi, hi;
      hi = euclidean(q,i);
      fi = ((hi % 2) + 2)%2;
      f.push_back(fi);
    }
    else if(metric=="cosine"){
      int fi, hi;
      hi = cosine(q,i);
      fi = hi;
      f.push_back(fi);
    }
  }

  memory_usage += sizeof(vector<int>) + (sizeof(int)*f.size()); // memory usage for stl vector that stores g functions

  //convert binary to key
  int firstkey = binary_vectorToDecimal(f);
  int key = firstkey;
  //cout << "Query point first key is " << key << endl;

  // while there M points to check and we still have probes to check , too , do
  while(current_M > 0 && current_probes > 0){
    // for each item in probe
    auto range = umap.equal_range(key);
    for(unordered_multimap <int,HashedPoint>::iterator it=range.first; it!=range.second; ++it){
      int point = it->second.getIndex(); // get the point in bucket
      double dist;
      if(metric == "euclidean")
        dist = euclidean_distance(&((*D).Dspace[point]),q); // the euclidean distance between p and q
      else if(metric == "cosine")
        dist = cosine_distance(&((*D).Dspace[point]),q); // the euclidean distance between p and q

      //cout << "Distance is " << dist << endl;

      if( R == 0)
        NearestNeighbors.push_back(point);
      if(dist < R )
        NearestNeighbors.push_back(point);

      current_M--;
      if(current_M == 0)
        break;
    }
    current_probes--;
    VerticesChecked.push_back(key);

    // if there still M points to be checked
    if( current_M > 0 && current_probes > 0){
      //cout << "I will need another prob " << endl;
      key = NearbyVectice(firstkey,VerticesChecked);
      //cout << key << endl;
    }
  }

  memory_usage += sizeof(vector<int>) + (sizeof(int)*VerticesChecked.size()); // memory usage for stl vector that stores vertices checked
  memory_usage += sizeof(vector<int>) + (sizeof(int)*NearestNeighbors.size()); // memory usage for stl vector that stores nns

  return NearestNeighbors;
}

// Find nearby verices with haming distance , that hasn't been already checked
int HashTable::NearbyVectice(int vertice,vector<int>& checked){
  unordered_multimap <int,HashedPoint>::iterator itr;
  int temp=-1,min = 1000000;
  int nearbyVert = -1;
  for (itr = umap.begin(); itr != umap.end(); itr++){
    if( temp == itr->first)
      continue;

    if(hamming_distance(vertice,itr->first) < min){
      if(find(checked.begin(), checked.end(),itr->first)==checked.end()){
        min = hamming_distance(vertice,itr->first);
        nearbyVert = itr->first;
      }
    }

    temp = itr->first;
  }
  return nearbyVert;
}

// Check if g family , exists in hash table
bool HashTable::existsG(vector<int>& g2){
  unordered_multimap <int,HashedPoint>::iterator itr;
  for (itr = umap.begin(); itr != umap.end(); itr++){
      if(itr->second.getG() == g2)
        return false;
   }
   return true;
}

// Print the hash table
void HashTable::printHashTable(){
  cout << "\n Hash Table contains : \n";
  unordered_multimap <int,HashedPoint>::iterator itr;
  for (itr = umap.begin(); itr != umap.end(); itr++)
  {
      cout << "------------------------------------------" << endl;
      cout << "Fi: " << itr->first << endl ;
      itr->second.printHashedPoint();
   }
}
