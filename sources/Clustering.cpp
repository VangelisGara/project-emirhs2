#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>
#include "../headers/VectorSpace.h"
#include "../headers/Clustering.h"
#include "../headers/MathHelper.h"

#define ITERATIONS 2

using namespace std;

// Lower Bound helper function
bool cmp(const pair<double,int> &l,const pair<double,int> &r) {
    return l.first < r.first;
}

// Clustering constructor
Clustering::Clustering(string input_file,string configuration_file,string init_method,string assign_method,string update_method,string metric):D(input_file){
  // Set configurations from conf. file
	ifstream conf_file(configuration_file);
	string variable;
	int value;
	while(conf_file >> variable >> value){
		if(variable=="number_of_clusters")
			noClusters = value;
		if(variable=="number_of_hash_tables")
			L = value;
		if(variable=="number_of_hash_functions")
			k = value;
    if(variable=="number_of_probes")
      probes = value;
    if(variable=="number_of_points")
      M = value;
	}

  // Explain what are we gonna do
  cout << "---------------------------------------------" << endl;
  cout << " Clustering vector space , " << endl;
  cout << " " << init_method << " as initialization method , " << endl;
  cout << " " << assign_method << " as assign method , " << endl;
  cout << " " << update_method << " as update method , " << endl;
  cout << " " << "and will produce "<< noClusters << " clusters."<< endl;
  cout << "---------------------------------------------" << endl;

  // Create hash tables if hashing assign method specified
  if(assign_method == "lsh"){
    vector<int> r; // r family same for each hash table
    for(int i=0; i<k ; i++)
      r.push_back((rand() % 10)); // create the r family
    for(int i=0; i<L; i++){ // create L hash tables
      HashTable Li(k,r,D.getDimension()); // create a hash table
      LTables.push_back(Li); // push it to the vector with hash tables
      LTables[i].HashVectorSpace(&D,k,metric,assign_method); // fill each hash table after hashing a vector space
      //LTables[i].printHashTable();
    }
  }

  else if(assign_method == "cube"){
    int d2 = floor(log2(D.getNumberOfPoints()));// k will be n for hybercube , in order to compute d'
    vector<int> r; // r family same for each hash table
    for(int i=0; i<k ; i++)
      r.push_back((rand() % 10)); // create the r family
    HashTable L(d2,r,D.getDimension()); // create a hash table
    LTables.push_back(L);
    LTables[0].HashVectorSpace(&D,D.getNumberOfPoints(),metric,assign_method); // fill it after hashing a vector space
    //L.printHashTable();
  }

  // Initialization phase:

  // Radnom Initialization
  if(init_method=="random"){
    //cout << "Centroids will be initialized with random selection" << endl;
    // Centroid will be point's index in the vector array
    for(int i=1; i<=noClusters ;i++){
      int random_point = uniformDistribution(0,D.getNumberOfPoints()-1);
      centroids.push_back(random_point);
      centroids_coords.push_back(D.Dspace[random_point]);
    }
    // Print them
    /*
    cout << "Starting with centroids:" << endl;
    for(int i=0; i<noClusters ;i++){
      cout << centroids[i] << " ";
      for(int j=0; j<centroids_coords[i].size(); j++)
         cout << centroids_coords[i][j] << " ";
    }
    cout << endl;
    */
  }
  // KMeans++ Initialization
  else if(init_method == "kmeans++"){
    //cout << "Centroids will be initialized with kmeans++" << endl;
    // K-means++ Algorithm
    int random_point= uniformDistribution(0,D.getNumberOfPoints()-1);
    centroids.push_back(random_point); // Select a random point as centroid
    centroids_coords.push_back(D.Dspace[random_point]);
    int t = 1; // Number of current centroids
    int n = D.getNumberOfPoints(); // Number of points in vector space
    int k = noClusters; // Number of clusters
    while(t < k){ // Until we get centroids required
      vector< pair<double,int> > P; // Store partial sums
      P.push_back( make_pair(0.0,-1) );
      double partial_sum;
      for(int i=0; i<n; i++){ // For every non-centroid
        if(find(centroids.begin(), centroids.end(),i)!=centroids.end()) // Ignore centroids
          continue;
        // Min. distance of current point to the rest centroids
        double Di=1000000;
        double current_dist;
        for(int c=0; c<t; c++){ // Compare point with the each centroid
          if(metric == "euclidean")
            current_dist = euclidean_distance(&(D.Dspace[i]),&(D.Dspace[centroids[c]]));
          else if(metric == "cosine")
            current_dist = cosine_distance(&(D.Dspace[i]),&(D.Dspace[centroids[c]]));
          if(current_dist < Di)
            Di = current_dist;
        }
        // Calculate partial sum
        partial_sum = Di*Di + P.back().first;
        P.push_back(make_pair(partial_sum,i));
      }
      // Pick a x uniformly on [0,P(n-t)]
      double x = uniformDistribution(0.1,P.back().first);
      // Pick new centroid re[1,2,...,n-t]: P(r-1) < x <= P(r)
      int r = lower_bound(P.begin(), P.end(), make_pair(x,0),cmp) - P.begin();
      centroids.push_back(P[r].second); // Add the new centroid
      centroids_coords.push_back(D.Dspace[P[r].second]);
      t = t + 1; // Now we have a new centroid
    }
    // Print them
    /*
    cout << "Starting with centroids:" << endl;
    for(int i=0; i<noClusters ;i++){
      cout <<  endl << endl << centroids[i] << endl << endl;;
      for(int j=0; j<centroids_coords[i].size(); j++)
         cout << centroids_coords[i][j] << " ";
   }
    cout << endl; getchar();
    */
  }

}

// Clustering constructor
Clustering::~Clustering(){
  //cout << "Clustering ends" << endl;
}

// Setters
void Clustering::setNoClusters(int number_of_clusters){
  noClusters = number_of_clusters;
}

// Getters
int Clustering::getNoClusters(){
  return noClusters;
}

// Find the two closest centroids
double Clustering::closest_centroids_dist(string metric){
  double current_dist,Di=1000000;
  for(int c1=0; c1<centroids.size(); c1++){ // for every centroid
    for(int c2=0; c2<centroids.size(); c2++){ // compute distance with all others
      if( centroids[c1] == centroids[c2] )
        continue;
      if(metric == "euclidean")
        current_dist = euclidean_distance(&(D.Dspace[centroids[c1]]),&(D.Dspace[centroids[c2]]));
      if(metric == "cosine")
        current_dist = cosine_distance(&(D.Dspace[centroids[c1]]),&(D.Dspace[centroids[c2]]));
      if(current_dist<Di)
        Di = current_dist;
    }
  }
  return current_dist;
}

// Assing each point in vector space to a cluster
void Clustering::AssignPoints(string assign_method,string metric){
  clusters.clear(); // new assignments , new clusters

  // Assign with Loyd's
  if( assign_method == "lloyd" ){
    for(int i=0; i< D.getNumberOfPoints(); i++){ // For every point
      double current_dist,Di=1000000;
      int nearest_centroid;
      for(int c=0; c<centroids.size(); c++){ // Find the nearest centroid
        if(metric == "euclidean")
          current_dist = euclidean_distance(&(D.Dspace[i]),&(centroids_coords[c]));
        else if(metric == "cosine")
          current_dist = cosine_distance(&(D.Dspace[i]),&(centroids_coords[c]));
        if(current_dist < Di){
          Di = current_dist;
          nearest_centroid = c; // store the nearest centroid to point examined i
        }
      }
      clusters.push_back(nearest_centroid);
    }
    vector<int> *ids=D.getIds();
    // Finished Assignment
    /*for(int i=0; i<D.getNumberOfPoints(); i++){
      cout << "Point with ID: " << (*ids)[i] << " belongs to cluster " << clusters[i] << endl;
    }*/
  }
  // Assign with LSH
  if( assign_method == "lsh" ){
    // Assign points with LSH
    vector<double> distance; // distance of each point from centroid
    vector<int> unassign_point; // will mark the changes
    for(int i=0; i<D.getNumberOfPoints(); i++){
      clusters.push_back(-1); // initialize the clusters each point belongs
      distance.push_back(1000000); // point's distance to cluster
      unassign_point.push_back(1);
    }

    // LSH Clustering
    double radius = closest_centroids_dist(metric)/2; // Min distance between centroids divided by 2
    for(int it=0; it<ITERATIONS; it++){
      //cout << radius << endl;getchar();
      for(int c=0; c<centroids.size(); c++){ // For every centroid
        // Range Search
        vector<int> Result,results; // range search's results
        for(int l=0 ; l<L ; l++){ // for each table
          results = (LTables)[l].RangeSearch(radius,&D,&(centroids_coords[c]),k,D.getNumberOfPoints(),metric); // get nearest neighbors
          for(uint j=0 ; j < results.size() ; j++)
            Result.push_back(results[j]); // get all neighbors
        }
        sort( Result.begin(), Result.end() ); // remove neighbors that are in list many times
        Result.erase(unique( Result.begin(), Result.end() ), Result.end() );

        // Assign each point returned from range search to bucket's closest centroid
        for(uint p1 = 0 ; p1 < Result.size() ; p1++ ){
          if(unassign_point[Result[p1]]){
            //cout << Result[p1] << endl; // the id of point in same bucket with centroids
            double current_dist;
            if(metric == "euclidean")
              current_dist = euclidean_distance(&(D.Dspace[Result[p1]]),&(centroids_coords[c]));
            else if(metric == "cosine")
              current_dist = cosine_distance(&(D.Dspace[Result[p1]]),&(centroids_coords[c]));
            if(current_dist < distance[Result[p1]]){
              distance[Result[p1]] = current_dist;
              clusters[Result[p1]] = c;
            }
          }
        }
      }
      for(int i=0; i<D.getNumberOfPoints(); i++){ // Mark assigned points
        if(clusters[i] > 0)
          unassign_point[i] = 0;
      }
      radius = radius*2;
    }

    /*
    // Print Clustering Results
    int unassigned_points = 0;
    for(int i=0; i<D.getNumberOfPoints(); i++){
      if(unassign_point[i] == 1)
        unassigned_points++;
      cout << "point " << i << " " << clusters[i] << " " <<  distance[i] << endl;
    }
    cout << unassigned_points << " unassigned points" << endl;
    getchar();
    */

    // For the unassigned points do lloyd's
    for(int i=0; i<D.getNumberOfPoints(); i++){
      if(unassign_point[i]){
        double current_dist,Di=1000000;
        int nearest_centroid;
        for(int c=0; c<centroids.size(); c++){ // Find the nearest centroid
          if(metric == "euclidean")
            current_dist = euclidean_distance(&(D.Dspace[i]),&(centroids_coords[c]));
          else if(metric == "cosine")
            current_dist = cosine_distance(&(D.Dspace[i]),&(centroids_coords[c]));
          if(current_dist < Di){
            Di = current_dist;
            nearest_centroid = c; // store the nearest centroid to point examined i
          }
        }
        clusters[i] = nearest_centroid;
      }
    }
    vector<int> *ids=D.getIds();
    // Finished Assignment
    /*for(int i=0; i<D.getNumberOfPoints(); i++){
      cout << "Point with ID: " << (*ids)[i] << " belongs to cluster " << clusters[i] << endl;
    }*/
  }
  // Assign with Hypercube
  if( assign_method == "cube" ){
    // Assign points with hybercube
    vector<double> distance; // distance of each point from centroid
    vector<int> unassign_point; // will mark the changes
    for(int i=0; i<D.getNumberOfPoints(); i++){
      clusters.push_back(-1); // initialize the clusters each point belongs
      distance.push_back(1000000); // point's distance to cluster
      unassign_point.push_back(1);
    }

    // Hypercube Clustering
    double radius = closest_centroids_dist(metric)/2; // Min distance between centroids divided by 2
    for(int it=0; it<ITERATIONS; it++){
      //cout << radius << endl;getchar();
      for(int c=0; c<centroids.size(); c++){ // For every centroid
        // Range Search
        vector<int> Result,results; // range search's results
        Result = (LTables)[0].RangeSearchCube(radius,&D,&(centroids_coords[c]),M,probes,D.getNumberOfPoints(),metric); // get nearest neighbors
        sort( Result.begin(), Result.end() ); // remove neighbors that are in list many times
        Result.erase(unique( Result.begin(), Result.end() ), Result.end() );

        // Assign each point returned from range search to bucket's closest centroid
        for(uint p1 = 0 ; p1 < Result.size() ; p1++ ){
          if(unassign_point[Result[p1]]){
            //cout << Result[p1] << endl; // the id of point in same bucket with centroids
            double current_dist;
            if(metric == "euclidean")
              current_dist = euclidean_distance(&(D.Dspace[Result[p1]]),&(centroids_coords[c]));
            else if(metric == "cosine")
              current_dist = cosine_distance(&(D.Dspace[Result[p1]]),&(centroids_coords[c]));
            if(current_dist < distance[Result[p1]]){
              distance[Result[p1]] = current_dist;
              clusters[Result[p1]] = c;
            }
          }
        }
      }
      for(int i=0; i<D.getNumberOfPoints(); i++){ // Mark assigned points
        if(clusters[i] > 0)
          unassign_point[i] = 0;
      }
      radius = radius*2;
    }

    /*
    // Print Clustering Results
    int unassigned_points = 0;
    for(int i=0; i<D.getNumberOfPoints(); i++){
      if(unassign_point[i] == 1)
        unassigned_points++;
      cout << "point " << i << " " << clusters[i] << " " <<  distance[i] << endl;
    }
    cout << unassigned_points << " unassigned points" << endl;
    getchar();
    */

    // For the unassigned points do lloyd's
    for(int i=0; i<D.getNumberOfPoints(); i++){
      if(unassign_point[i]){
        double current_dist,Di=1000000;
        int nearest_centroid;
        for(int c=0; c<centroids.size(); c++){ // Find the nearest centroid
          if(metric == "euclidean")
            current_dist = euclidean_distance(&(D.Dspace[i]),&(centroids_coords[c]));
          else if(metric == "cosine")
            current_dist = cosine_distance(&(D.Dspace[i]),&(centroids_coords[c]));
          if(current_dist < Di){
            Di = current_dist;
            nearest_centroid = c; // store the nearest centroid to point examined i
          }
        }
        clusters[i] = nearest_centroid;
      }
    }
    vector<int> *ids=D.getIds();
    // Finished Assignment
    /*for(int i=0; i<D.getNumberOfPoints(); i++){
      cout << "Point with ID: " << (*ids)[i] << " belongs to cluster " << clusters[i] << endl;
    }*/
  }
}

// Update centroids by the new clusters
int Clustering::UpdateCentroids(string update_method,string metric){
  // KMeans update method
  if(update_method == "kmeans"){
    //cout << "We will update centroids with kmeans" << endl;
    vector<vector <double> > new_centroids; // new centroids will be inserted here
    // For each cluster create a new point as a centroid
    for(int i=0; i<noClusters; i++){
      vector<double> new_centroid;
      for(int j=0; j<D.getDimension(); j++) // the new centroid will be point [0,0,0,...,0]
        new_centroid.push_back(0);
      new_centroids.push_back(new_centroid);
    }
    // Start kmean computing
    for(int i=0; i<D.getNumberOfPoints(); i++){ // for every point
      int points_cluster = clusters[i]; // the cluster the i point belongs
      for(int j=0; j<D.getDimension(); j++) // calculate mean dimension
          new_centroids[points_cluster][j] += D.Dspace[i][j];
    }
    // Compute the mean
    for(int i=0; i<noClusters; i++){
      int cluster_size = count(clusters.begin(),clusters.end(),i);
      for(int j=0; j<D.getDimension(); j++) // calculate mean dimension
        new_centroids[i][j] =  new_centroids[i][j]/cluster_size;
      centroids[i] = -1;
    }
    // Print the coordinates of new centroids
    /*
    for(int i=0; i<noClusters; i++){
      cout << "Centroid " << i << endl;
      for(int j=0; j<D.getDimension(); j++) // calculate mean dimension
        cout << new_centroids[i][j] << " ";
      cout << endl;
    }
    getchar();
    */

    if(centroids_coords == new_centroids) // centroids didn't change
      return 1;

    centroids_coords = new_centroids; // define new centroids
    return 0;
  }
  // PAM update method
  else if(update_method == "pam"){
    //cout << "We will update centroids with pam" << endl;
    vector<vector <double> > new_centroids; // new centroids will be inserted here
    vector<double> medoid_distance; // the medoids distance from rest points in cluster
    for(int t=0; t<D.getNumberOfPoints(); t++)
      medoid_distance.push_back(0);
    for(int t=0; t<D.getNumberOfPoints(); t++){ // for each point
      // compute the distance of this medoid from the rest in the same cluster
      for(int i=0; i<D.getNumberOfPoints(); i++){
        if(clusters[t] == clusters[i]){
          if(metric == "euclidean")
            medoid_distance[t] += euclidean_distance(&(D.Dspace[i]),&(D.Dspace[t]));
          else if(metric == "cosine")
            medoid_distance[t] = cosine_distance(&(D.Dspace[i]),&(D.Dspace[t]));
        }
      }
    }
    vector<double> min_distance;
    for(int i=0; i<noClusters; i++)
      min_distance.push_back(1000000);
    for(int t=0; t<D.getNumberOfPoints(); t++){ // find the medoid that minimizes distance from the rest
      if(medoid_distance[t] < min_distance[clusters[t]] ){
        min_distance[clusters[t]] = medoid_distance[t];
        centroids[clusters[t]] = t; // this is the point (medoid) with the min distance in cluster
      }
    }
    for(int i=0; i<noClusters; i++)
      new_centroids.push_back(D.Dspace[centroids[i]]);

    if(centroids_coords == new_centroids) // centroids didn't change
      return 1;

    centroids_coords = new_centroids; // define new centroids
    return 0;

  }
}

// Complete clustering algorithm based on combination
void Clustering::startClustering(string output_file,bool complete,string init_method,string assign_method,string update_method,string metric){
  //cout << "Starting clustering points with " << assign_method << " assignment method and metric " << metric << endl;
  auto start_time = chrono::high_resolution_clock::now(); // start counting time
  int changed = 0;
  int number_of_iterations = 0;
  do{
    // Assign points to clusters
    AssignPoints(assign_method,metric);
    // Update centroids
    changed = UpdateCentroids(update_method,metric);
    number_of_iterations++;
  }while( (!changed) && (number_of_iterations<50) ); // Stopping criterion , if centroids didn't change
  // Evaluate Clustering
  double score = ClusteringEvaluation("silhouette",metric);
  auto current_time = chrono::high_resolution_clock::now();
  cout << score << endl;

  // Print results to file
  ofstream out;
  out.open(output_file,ios::app);
  out << "---------------------------------------------------------------------------------" << endl;
  out << "Algorithm:" << " 1." << init_method << " 2." << assign_method << " 3." << update_method << endl;
  out << "Metric: " << metric << endl;
  out << "---------------------------------------------------------------------------------" << endl;
  if(!complete){
    // compute the size of each cluster
    int cluster_size[noClusters];
    for(int i=0; i<noClusters; i++)
      cluster_size[i] = 0;
    for(int i=0; i<D.getNumberOfPoints(); i++){
      cluster_size[clusters[i]]++;
    }
    for(int i=0; i<noClusters; i++){
      out << "CLUSTER-" << i << endl;
      out << "{size: " << cluster_size[i] << "," << endl;
      out << "centroid: ";
      if(update_method!="kmeans")
        out << centroids[i] << "}" << endl;
      else{
        for(int j=0; j<centroids_coords[i].size(); j++)
          out << centroids_coords[i][j] << " ";
        out << "}" << endl;
      }
      out << endl;
    }
  }
  else{
    vector<int>* ids = D.getIds();
    for(int i=0; i<noClusters; i++){
      out << "CLUSTER-" << i << endl;
      out << "{";
      for(int j=0; j<D.getNumberOfPoints(); j++){
        if( clusters[j] == i)
          out << (*ids)[j] << ",";
      }
      out << "}" << endl;
    }
  }
  out << "clustering_time: ";
  out << chrono::duration_cast<chrono::seconds>(current_time - start_time).count() << " seconds" << endl;
  out << "Silhouette: [" ;
  for(int i =0; i<noClusters; i++){
    out << cluster_score[i] << ",";
  }
  out << score << "]" << endl;
  out << "---------------------------------------------------------------------------------" << endl;
  out.close();
}

// Evaluation of our clustering
double Clustering::ClusteringEvaluation(string evaluation_method,string metric){
  if(evaluation_method=="silhouette"){
    double mean_silhouette = 0;
    for(int i=0; i<noClusters; i++){ // initialize score for each cluster
      cluster_score.push_back(0);
    }
    for(int i=0; i<D.getNumberOfPoints(); i++){ // for each cluster
      // point's distance from centroid assigned
      double a;
      if(metric == "euclidean")
        a = euclidean_distance(&(D.Dspace[i]),&(centroids_coords[clusters[i]]));
      else if(metric == "cosine")
        a = cosine_distance(&(D.Dspace[i]),&(centroids_coords[clusters[i]]));
      // point's distance from second closest centroid
      double b = 1000000;
      double current_b;
      for(int c=0; c<noClusters; c++){ // calculate a,b
        if( clusters[i] == c)
          continue;
        if(metric == "euclidean")
          current_b = euclidean_distance(&(D.Dspace[i]),&(centroids_coords[c]));
        else if(metric == "cosine")
          current_b = cosine_distance(&(D.Dspace[i]),&(centroids_coords[c]));
        if( current_b < b)
          b = current_b;
      }
      // calculate silhouette
      double silhouette = (b-a)/max(a,b);
      if(isnan(silhouette))
        silhouette = 0;
      cluster_score[clusters[i]] += silhouette;
      mean_silhouette += silhouette;
    }
    // compute the size of each cluster
    int cluster_size[noClusters];
    for(int i =0; i<noClusters; i++)
      cluster_size[i] = 0;
    for(int i=0; i<D.getNumberOfPoints(); i++){
      cluster_size[clusters[i]]++;
    }
    for(int i =0; i<noClusters; i++){ // silhouette score for each cluster
      if(cluster_size[i] != 0)
        cluster_score[i] = cluster_score[i]/cluster_size[i];
    }
    mean_silhouette = mean_silhouette/D.getNumberOfPoints();
    return mean_silhouette;
  }
}


/************************************
             Notes
*************************************
         1.Divide by max
*************************************/
