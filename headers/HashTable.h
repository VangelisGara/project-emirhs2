#ifndef HASHTABLE_H
#define HASHTABLE_H

using namespace std;

#include <unordered_map>
#include <string>
#include <vector>
#include <list>
#include "../headers/VectorSpace.h"

// Will be stored to buckets
class HashedPoint{
  int index; // the point's index in vector space , aka the p point
  vector<int> g; // the g function that came from
  public:
    HashedPoint(int indx);
    HashedPoint(int indx ,vector<int>* g_func , int k); // contructor
    ~HashedPoint(); // destructor
    //Getter
    int getIndex();
    vector <int> getG();
    void printHashedPoint(); // prints the has
};

// The hash table containing the buckets
class HashTable{
  unordered_multimap <int,HashedPoint> umap; // <key: the fi function , value: the point and the g that came from
  // the rk values for the Fi function
  vector<int> r;
  const int w=2;
  // each hi function should have the same v and t
  vector<vector<double>> v;
  vector<float> t;
  public:
    HashTable(int k,vector<int> initial_r,int dim=0); // constructor
    ~HashTable(); // destructor
    void HashPoint(int k ,int indx , vector <double>* p,int n,string metric,string alg); // hash a point
    void HashVectorSpace(VectorSpace* D,int k,string metric,string alg="lsh"); // hash all points in a vector space
    vector<int> RangeSearch(double R,VectorSpace* D,vector <double>* q,int k,int n,string metric,int c=1); // range search algorithm in hash table
    vector<int> RangeSearchCube(double R,VectorSpace* D,vector <double>* q,int M,int probes,int k,string metric); // range search algorithm for cube algorithm
    int NearbyVectice(int vertice,vector<int>& checked); // return the closest neighbor based on hamming distance
    bool existsG(vector<int>& g2); // check if g is already assigned to a point in hash table
    void printHashTable();
  protected:
    int euclidean(vector <double>*p,int i); // metric used , the LSH family
    int cosine(vector <double>*p,int i);
};

#endif
