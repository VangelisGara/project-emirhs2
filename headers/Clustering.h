#ifndef CLUSTERING_H
#define CLUSTERING_H

using namespace std;

#include <iostream>
#include <fstream>
#include <math.h>
#include "../headers/VectorSpace.h"
#include "../headers/HashTable.h"

class Clustering{
  int noClusters; // number of clusters
  vector<int> centroids; // the centroids
  vector<vector <double>> centroids_coords; // coordinates of centroids
  vector<double> cluster_score; // the evaluation score for each cluster
  vector<int> clusters; // the cluster each point belongs
  // For LSH
  vector<HashTable> LTables;
  int L=4,k=5;
  // For Hypercube
  int probes=10,M=100;
  public:
    VectorSpace D; // the vector space the clustering will occure
    Clustering(string input_file,string configuration_file,string init_method,string assign_method,string update_method,string metric); // constructor
    ~Clustering(); // destructor
    void setNoClusters(int number_of_clusters); // setters
    int getNoClusters(); // getters
    double closest_centroids_dist(string metric); // the distance of the closest centroids
    void startClustering(string output_file,bool complete,string init_method,string assign_method,string update_method,string metric);
    void AssignPoints(string assign_method,string metric); // assign each point to a cluster
    int UpdateCentroids(string update_method,string metric); // update centroids
    double ClusteringEvaluation(string evaluation_method,string metric); // evaluation of the clustering
};

#endif
