#ifndef MATHHELPER_H
#define MATHHELPER_H

using namespace std;

// uniform distribution generator
double uniformDistribution(double a,double b);
// v point with coords based on normal distribution
float normalDistribution();
// the dot product of two points
double dotProduct(vector<double>* a, vector<double>* b);
// euclidean distance
double euclidean_distance(vector <double>* a,vector <double>* b);
// cosine distance
double cosine_distance(vector <double> *a,vector <double> *b);
// haming distance between two integers
int hamming_distance(int x, int y);
// convert binary to integer , where binary is stored into a vector
int binary_vectorToDecimal(vector<int>& binary);

#endif
