#ifndef VECTORSPACE_H
#define VECTORSPACE_H

#include <vector>

using namespace std;

// the vector space where all points from text file will be stored to
class VectorSpace{
  int dimension,number_of_points;
  vector<int> itemIds; // the id of each point

  public:
    vector <vector <double>> Dspace; // the dimensional space D , containing d-dimensional
    VectorSpace(string input_file); // constructor
    ~VectorSpace(); // destructor
    // Getters
    int getDimension();
    int getNumberOfPoints();
    vector<int>* getIds();
    int NearestNeighbor(vector <double>* q,string metric); // returns the nearest point to q
    void printPoint(int i); // print a point from vector space
    void printVectorSpace(); // print the vector space
};

#endif
