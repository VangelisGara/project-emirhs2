CC=g++

SRC := sources
OBJ := objects

SOURCES := $(wildcard $(SRC)/*.cpp)
OBJECTS := $(patsubst $(SRC)/%.cpp, $(OBJ)/%.o, $(SOURCES))

cluster: $(OBJECTS)
	$(CC) -g -Wall algorithm/cluster.cpp $^ -o $@

$(OBJ)/%.o: $(SRC)/%.cpp
	$(CC) -g -I$(SRC) -c $< -o $@

clean:
	@rm -f D ./objects/*.o core
	@rm -f cluster
	@rm -f
