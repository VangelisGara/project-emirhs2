#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include "../headers/VectorSpace.h"
#include "../headers/Clustering.h"

using namespace std;

int memory_usage;

int main(int argc, char *argv[]){
	memory_usage = 0; // memory usage for clustering

	// Configurations
	string configuration_file,output_file,input_file,metric;
	bool complete = false;

	// Clustering configuration
	string initialization_method[] = {"random","kmeans++"};
	string assign_method[] = {"lloyd","lsh","cube"};
	string update_method[] = {"kmeans","pam"};

	// Set configurations from args.
	vector<string> Arguments(argv, argv + argc); // Store Argument Here
	for( int i = 0; i < argc; i++){
    		if( Arguments[i] == "-d")
      			metric = Arguments[i+1];
    		if( Arguments[i] == "-c")
      			configuration_file = Arguments[i+1];
    		if( Arguments[i] == "-o")
      			output_file = Arguments[i+1];
    		if( Arguments[i] == "-i")
      			input_file = Arguments[i+1];
    	  if( Arguments[i] == "-complete")
          	complete = true;
	}

	// Clustering configuration from user
	string answer;
	cout << "• Specify clustering's configuration ? [yes/no]" << endl << "> ";
	cin >> answer;
	if( answer == "yes"){
		cout << endl << "• Please select configurations: e.g.[132]" << endl;
		cout << " ⚬Initialization :" << endl << "	1.Random" << endl << "	2.Kmeans++" << endl;
		cout << " ⚬Assignment :" << endl << "	1.Lloyd" << endl << "	2.LSH" << endl <<  "	3.Hypercube" << endl;
		cout << " ⚬Update :" << endl << "	1.Kmeans" << endl << "	2.PAM" << endl << "> ";
		cin >> answer;
		string init,assign,update;
		init = initialization_method[((int)answer[0]-'0')-1];
		assign = assign_method[((int)answer[1]-'0')-1];
		update = update_method[((int)answer[2]-'0')-1];
		//cout << init << " " << assign << " " << update << endl; getchar();

		// Prepare for the clustering
		Clustering clustering(input_file,configuration_file,init,assign,update,metric);
		// Start clustering
		clustering.startClustering(output_file,complete,init,assign,update,metric);
		cout << "✔️" << endl;
	}
	else { // run clustering with all possible combinations
		for(int im=0; im<2; im++){
			for(int am=0; am<3; am++){
				for(int um=0; um<2; um++){
					// Prepare for the clustering
					Clustering clustering(input_file,configuration_file,initialization_method[im],assign_method[am],update_method[um],metric);
					// Start clustering
					clustering.startClustering(output_file,complete,initialization_method[im],assign_method[am],update_method[um],metric);
					cout << "✔️" << endl;
				}
			}
		}
	}
}
